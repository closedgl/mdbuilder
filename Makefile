all: mdbuilder

mdbuilder: main.o markdown.o inputfile.o
	cc *.o -o mdbuilder

main.o: src/main.c
	cc -c src/main.c

markdown.o: src/markdown.c
	cc -c src/markdown.c

inputfile.o: src/inputfile.c
	cc -c src/inputfile.c

clean:
	rm *.o
