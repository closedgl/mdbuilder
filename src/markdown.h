#ifndef mdb_markdown
#define mdb_markdown

#include <stdint.h>

// output options
void strong_em_tags();
void skim_for_heading();

// prettyprinting
extern uint8_t prettyprint;
void pp_newline();
void pp_indent();

// "hierarchy" of markdown file
void mdconvert_all(char term);
long int mdconvert_line(long int index);
long int mdconvert(char term, int8_t min_term, long int index);

// markdown elements
long int md_paragraph(long int index);
long int md_blockquote(long int index);
long int md_heading(long int index);
long int md_unordered_list(long int index);
long int md_ordered_list(long int index);
long int md_list_item(long int index);
long int md_link(long int index);
long int md_image(long int index);
long int md_code(long int index);
long int md_bold_italic(long int index);
long int md_superscript(long int index);
long int md_spaces(long int index);
long int md_code_block(long int index);
long int md_sub_strike(long int index);

// Is this a markdown element?
uint8_t is_hr_or_li(long int index);
#define is_hr 1
#define is_li 2
uint8_t is_number(long int index);
uint8_t is_ordered_li(long int index);
uint8_t is_link(long int index);
uint8_t is_code_block(long int index);

// moving around
long int skip_whitespace(long int index);
long int skip_no_whitespace(long int index);
long int find_close_bracket(long int index);

#endif // mdb_markdown
