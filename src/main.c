#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include "markdown.h"
#include "inputfile.h"

void
usage(char* cmd)
{
	fprintf(stderr, "usage: %s -i mdfile [options]\n\n", cmd);
	fprintf(stderr, "options:\n");
	fprintf(stderr, "  -i mdfile    Markdown file to read\n");
	fprintf(stderr, "                 Reads from stdin if not specified\n");
	fprintf(stderr, "  -p           Prettyprint output with tabs and returns\n");
	fprintf(stderr, "  -t           Only output text inside first heading (title)\n");
	fprintf(stderr, "  -h           Display this help and exit\n");
	fprintf(stderr, "  -s           Use <strong> and <em> for bold and italic text\n");

	exit(1);
}

int
main(int argc, char** argv)
{
	uint8_t title = 0;
	char* input_file = NULL;
	FILE* fd;

	// not enough arguments?
	if (argc < 2) usage(argv[0]);

	// process arguments
	int warg, ws, arglen; // working argument, working switch, argument length
	for (warg = 1; warg < argc; warg++) {
		arglen = strlen(argv[warg]);
		if (argv[warg][0] == '-') {
			for (ws = 1; ws < arglen; ws++) {
				switch (argv[warg][ws]) {
					case 'i': // input file
						if (warg + 1 == argc) {
							fprintf(stderr, "-i: missing value\n");
							usage(argv[0]);
						}
						input_file = argv[warg + 1];
						break;

					case 'p': // prettyprint
						prettyprint = 1;
						break;

					case 't': // only output title
						title = 1;
						break;

					case 's': // use <strong> and <em>
						strong_em_tags();
						break;

					case 'h': // help
						usage(argv[0]);

					default: // unknown
						usage(argv[0]);
				}
			}
		}
	}

	// open input file or stdin
	if (input_file == NULL)
		open_input_fd(stdin);
	else
		open_input(input_file);

	// conversion
	if (title)
		skim_for_heading();
	else
		mdconvert_all('\0');

	close_input();
	return 0;
}
