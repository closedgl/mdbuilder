#ifndef mdb_inputfile
#define mdb_inputfile

#include <stdio.h>
#include <stdint.h>

typedef struct {
	FILE* fd;
	long int size;
	char* buf;
} mdstring;

extern mdstring input_file;

void open_input(char* path);
void open_input_fd(FILE* fd);
void close_input();

#endif // mdb_inputfile
