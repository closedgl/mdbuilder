#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>

#include "inputfile.h"

mdstring input_file;

void
open_input(char* path)
{
	// open file from path
	FILE* fd = fopen(path, "r");
	if (fd == NULL) {
		perror("Could not open input file");
		exit(errno);
	}

	open_input_fd(fd);
}

void
open_input_fd(FILE* fd)
{
	char c1, c2;
	long int i = 0;

	// init buffer
	input_file.fd = fd;
	input_file.buf = malloc(1);

	// read stream into buffer until EOF reached
	while ((c1 = fgetc(fd)) != EOF)
		switch (c1) {
		case '\r': // replace CR and CRLF with LF
			if ((c2 = fgetc(fd)) == '\n') {
				input_file.buf = realloc(input_file.buf, i+2);
				input_file.buf[i] = '\n';
				i++;
			} else {
				input_file.buf = realloc(input_file.buf, i+3);
				input_file.buf[i] = '\n';
				input_file.buf[i+1] = c2;
				i+=2;
			}
			break;
		default:
			input_file.buf = realloc(input_file.buf, i+2);
			input_file.buf[i] = c1;
			i++;
		}

	input_file.size = i - 1;
}

void
close_input()
{
	// release buffer
	free(input_file.buf);

	// close stream
	fclose(input_file.fd);
}
