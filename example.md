# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6
####### There is no Heading 7

---

This is *italic*. This is also _italic_.

This is **bold**. This is also __bold__.

This is ***bold and italic***.  
This is also ___bold and italic___.  
This should also be **_bold and italic_**.  
This too should be __*bold and italic*__.

This  
has  
line  
breaks.

Bash forkbomb: `:(){ :|:& };:`

- ul item 1
- ul item 2
	- indented li 1
	- indented li 2
- ul item 3
* unordered lists can start with \*.
+ they can also start with \+.

1. ol item 1
2. ol item 2
3. ol item 3

8. This ol should start at 8
9. and end at 9

# Chore list

- [X] load the dishwasher
- [X] vaccum the living room
- [X] dust shelves
- [ ] clean bathroom mirror
- [ ] organize my closet

[mdbuilder](https://codeberg.org/closedgl/mdbuilder "CLI Markdown to HTML converter"), a lightweight alternative to pandoc if you're just converting markdown to html

![image](image.png)

[![Linked image](link.png)](https://example.com)

> # Blockquote
> 
> The *quick* **cheetah** jumps
> over the ***lazy bunny***.
> 
> Paragraph 2
> 
> - Jelly HotDog
> - Candied ravioli
> - \[salad Salad\]
>
> 1. Draw circle
> 2. Draw rectangle
> 3. Draw rest of airplane
> 
>> Nested blockquote
>>
>>> Nested blockquote in nested blockquote
>>>
>>> Neovim's markdown syntax highlighting doesn't do nested blockquotes

[Reference style link][1]

[1]: <https://invidious.snopyta.org/watch?v=dQw4w9WgXcQ>

| Port | Protocol | Description |
| --- | --- | --- |
| 22 | SSH | Secure remote login |
| 80 | HTTP | Standard web protocol |
| 443 | HTTPS | Secure web protocol |

| Port | Protocol | Description |
| :--- | :---: | ---: |
| 22 | SSH | Secure remote login |
| 80 | HTTP | Standard web protocol |
| 443 | HTTPS | Secure web protocol |

```
int
main(int argc, char **argv)
{
	printf("Hello world\n");
}
```

~~~
int
main()
{
	puts("Hello world");
}
~~~

Global warming is a ~~hoax~~ serious threat.

There is ==some Markdown syntax== that Neovim's syntax highlighting ==doesn't support==.

a^2^ + b^2^ = c^2^

C~6~H~12~O~6~

---

[^1]: Markdown has a lot of different syntax
