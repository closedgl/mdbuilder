# mdbuilder

Command-line Markdown to HTML converter written in C

## Supported elements

### Basic syntax

- [X] Paragraph
- [X] Line break
- [X] Headings
- [X] Bold
- [X] Italic
- [X] Bold-italic
- [X] Unordered list
- [X] Ordered list
- [X] Blockquote
- [X] Code
- [X] Horizontal rule
- [X] Link
- [ ] Link title
- [X] Image
- [ ] Nested lists
- [X] Nested blockquotes
- [ ] Escaping syntax characters

### Extended syntax

- [ ] Table
- [X] Fenced code block
- [ ] Footnote
- [ ] Heading ID
- [ ] Definition list
- [X] Strikethrough
- [ ] Highlight
- [X] Subscript
- [X] Superscript
- [ ] Task list
